window.onload = function() { init() };
var sheetProjetos = 'https://docs.google.com/spreadsheets/d/1xcep11FO2J0t9rTbfK3Wm6tWEobclE29IDopyT7zuKU/edit?usp=sharing';

function init() {
    a = Tabletop({
        key: sheetProjetos,
        callback: showInfo,
        simpleSheet: true
    });
}

var count = 0;
function showInfo(data, tabletop) {
    var grupos = data.filter(function (valor, indice, arr) {
    var achou = false;
    for (var ai = 1 + indice, at = arr.length;ai < at;++ai) {    // percorre a lista em busca de elementos repetidos
        if (valor["Programa"] == arr[ai]["Programa"]) {        // identifica se o elemento atual (valor) existe na lista
            achou = true;
            break;
        }
    }
    return !achou;    // adiciona na lista apenas elementos NÃO encontrados
    }).map(function (valor, indice) {            
        return {id:indice, content:valor["Programa"], value:1+indice};
    }).sort();    // ordena a lista (opcional)

    var groupsDataset = new vis.DataSet(grupos);
    var parseData = function (str) {
        var s = str.split("/");
        return new Date(s[2],s[1] - 1,s[0]);
    }

    var dataPrograma = data.filter(function (valor) {
        return "Projeto" == valor["Tipo Marco"];
    }).map(function (valor, indice) {
        return {id: indice, group: grupos.filter(function (valor2) {
            return valor["Programa"] == valor2.content;
        })[0].id, content: valor["Projeto"] + ": " + valor["Data Início"] + " - "+ valor["Data Fim"], start: parseData(valor["Data Início"]), end: parseData(valor["Data Fim"])}
    });
    var dataINSS = data.filter(function (valor) {
        return (("INSS" == valor["Programa"]) &&  ("Projeto" != valor["Tipo Marco"]));
    }).map(function (valor, indice) {
        return {id: indice, group: grupos.filter(function (valor2) {
            return valor["Programa"] == valor2.content;
        })[0].id, id: valor["id"], content: valor["Projeto"] + " - " + valor["Descrição"] + ": " + valor["Data Início"], 
            start: parseData(valor["Data Início"])/*, type: 'point'*/}
    });
    
    var itemsDatasetPrograma = new vis.DataSet(dataPrograma);
    var itemsDatasetINSS = new vis.DataSet(dataINSS);

    /*Exibir dados do Programa*/
    var container = document.getElementById('visualization');
    var timeline = new vis.Timeline(container);
    timeline.setGroups(groupsDataset);
    timeline.setItems(itemsDatasetPrograma);        
    timeline.fit();

    /*Exibir dados do INSS*/
    var containerINSS = document.getElementById('visualizationINSS');
    var timelineINSS = new vis.Timeline(containerINSS);        
    timelineINSS.setItems(itemsDatasetINSS);        
    timelineINSS.fit();
}